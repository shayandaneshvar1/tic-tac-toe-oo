package ir.shayandaneshvar;

import ir.shayandaneshvar.presenter.View;
import javafx.application.Application;

public class Main {
    public static void main(String[] args) {
        Application.launch(View.class);
    }
}
