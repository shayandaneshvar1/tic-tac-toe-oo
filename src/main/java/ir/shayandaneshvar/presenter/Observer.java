package ir.shayandaneshvar.presenter;

import ir.shayandaneshvar.model.Observable;

public interface Observer<T extends Observable> {
    void update(T observable);
}
