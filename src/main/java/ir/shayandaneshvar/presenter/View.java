package ir.shayandaneshvar.presenter;

import ir.shayandaneshvar.model.Board;
import ir.shayandaneshvar.model.Cell;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class View extends Application implements Observer<Board> {


    @Override
    public void update(Board observable) {
        draw(observable.getCells());
    }

    private void draw(Cell[][] cells) {
        // TODO: 2/3/2020
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();
        Scene scene = new Scene(root);
        primaryStage.setTitle("Tic Tac Toe Game");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void stop() throws Exception {
        System.exit(0);
    }
}
