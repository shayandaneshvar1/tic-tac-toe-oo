package ir.shayandaneshvar.model;

public class Cell implements Cloneable {
    private Character value;

    public Cell() {
        value = ' ';
    }

    public Character getValue() {
        return value;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public void setValue(Character value) {
        this.value = value;
    }
}
