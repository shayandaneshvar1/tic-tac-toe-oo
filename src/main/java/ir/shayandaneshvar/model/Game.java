package ir.shayandaneshvar.model;

public class Game implements Runnable {
    private Board board;
    private Player player1;
    private Player player2;
    private Integer cycle = 0;

    public Game(Board board, Player player1, Player player2) {
        this.board = board;
        this.player1 = player1;
        this.player2 = player2;
    }

    @Override
    public void run() {
        // TODO: 2/3/2020
    }

    public boolean isEnded() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board.getCell(i, j).getValue() == ' ') {
                    return false;
                }
            }
        }
        return true;
    }


    private Character checkWin() {
        if (board.getCell(0, 0) == board.getCell(1, 1) &&
                board.getCell(1, 1) == board.getCell(2, 2)) {
            return board.getCell(0, 0).getValue();
        } else if (board.getCell(0, 0) == board.getCell(0, 1) &&
                board.getCell(0, 1) == board.getCell(0, 2)) {
            return board.getCell(0, 0).getValue();
        } else if (board.getCell(0, 0) == board.getCell(1, 0) &&
                board.getCell(1, 0) == board.getCell(2, 0)) {
            return board.getCell(0, 0).getValue();
        } else if (board.getCell(0, 2) == board.getCell(1, 1) &&
                board.getCell(1, 1) == board.getCell(2, 0)) {
            return board.getCell(1, 1).getValue();
        } else if (board.getCell(1, 0) == board.getCell(1, 1) &&
                board.getCell(1, 1) == board.getCell(1, 2)) {
            return board.getCell(1, 1).getValue();
        } else if (board.getCell(0, 1) == board.getCell(1, 1) &&
                board.getCell(1, 1) == board.getCell(2, 1)) {
            return board.getCell(1, 1).getValue();
        } else if (board.getCell(2, 0) == board.getCell(2, 1) &&
                board.getCell(2, 1) == board.getCell(2, 2)) {
            return board.getCell(2, 2).getValue();
        } else if (board.getCell(0, 2) == board.getCell(1, 2) &&
                board.getCell(1, 2) == board.getCell(2, 2)) {
            return board.getCell(2, 2).getValue();
        }
        return ' ';
    }

    public Player getWinner(char c) {
        if (player1.getPiece() == c) {
            return player1;
        } else if (player2.getPiece() == c) {
            return player2;
        }
        return null;
    }
}
