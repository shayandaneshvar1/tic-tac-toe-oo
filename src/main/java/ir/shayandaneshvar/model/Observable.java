package ir.shayandaneshvar.model;

import ir.shayandaneshvar.presenter.Observer;

public interface Observable {
    void addObserver(Observer observer);
    void updateObserver();
}
