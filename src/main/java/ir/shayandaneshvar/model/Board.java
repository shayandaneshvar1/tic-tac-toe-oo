package ir.shayandaneshvar.model;

import ir.shayandaneshvar.presenter.Observer;

import java.util.HashSet;
import java.util.Set;

public class Board implements Observable {
    private Cell[][] cells;
    private Set<Observer<Board>> observers;

    public Board() {
        cells = new Cell[3][3];
        observers = new HashSet<>();
        resetBoard();
    }

    public void resetBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                cells[i][j] = new Cell();
            }
        }
    }


    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public Cell[][] getCells() {
        return cells.clone();
    }

    public Cell getCell(int i, int j) {
        return cells[j][i];
    }

    public void updateObserver() {
        observers.forEach(x -> x.update(this));
    }
}
