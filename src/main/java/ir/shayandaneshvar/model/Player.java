package ir.shayandaneshvar.model;

import javafx.util.Pair;

public abstract class Player {
    private String name;
    private Character piece;

    public String getName() {
        return name;
    }

    public Character getPiece() {
        return piece;
    }

    public Integer getWins() {
        return wins;
    }

    public Integer getLoses() {
        return loses;
    }

    public Integer getDraws() {
        return draws;
    }

    private Integer wins;
    private Integer loses;
    private Integer draws;

    abstract Pair<Integer, Integer> play(Board board);

    public void addWin() {
        wins++;
    }

    public void addLose() {
        loses++;
    }

    public void addDraw() {
        draws++;
    }
}
